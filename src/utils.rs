use bevy_ecs_tilemap::prelude::*;

#[derive(Copy, Clone)]
pub(crate) struct Size {
    pub(crate) x: u32,
    pub(crate) y: u32,
}

impl Size {
    pub(crate) const SINGLE: Self = Self { x: 1, y: 1 };
}

pub(crate) fn is_colliding(
    a: TilePos,
    asize: Size,
    b: TilePos,
    bsize: Size,
) -> bool {
    b.x <= a.x + (asize.x - 1) / 2 + bsize.x / 2
        && a.x <= b.x + asize.x / 2 + (bsize.x - 1) / 2
        && b.y <= a.y + (asize.y - 1) / 2 + bsize.y / 2
        && a.y <= b.y + asize.y / 2 + (bsize.y - 1) / 2
}

pub(crate) enum Zindex {
    Tiles,
    FloorEntities,
    Entities
}

impl Into<f32> for Zindex {
    fn into(self) -> f32 {
        match self {
            Self::Tiles => 1.,
            Self::FloorEntities => 2.,
            Self::Entities => 3.
        }
    }
}
