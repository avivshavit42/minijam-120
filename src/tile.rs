use bevy::prelude::*;

use bevy_ecs_tilemap::prelude::*;

pub(crate) struct TilePlugin;

pub(crate) const TILE_SIZE: TilemapTileSize =
    TilemapTileSize { x: 16.0, y: 16.0 };

impl Plugin for TilePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TilemapPlugin);
    }
}
pub(crate) fn get_neighbors(pos: &TilePos, size: &TilemapSize) -> Vec<TilePos> {
    let right = || {
        Some(TilePos {
            x: pos.x + 1,
            y: pos.y,
        })
    };

    let left = || {
        Some(TilePos {
            x: pos.x.checked_sub(1)?,
            y: pos.y,
        })
    };

    let up = || {
        Some(TilePos {
            x: pos.x,
            y: pos.y + 1,
        })
    };

    let down = || {
        Some(TilePos {
            x: pos.x,
            y: pos.y.checked_sub(1)?,
        })
    };


    [right(), left(), up(), down()].into_iter()
        .flatten()
        .filter(|pos| pos.within_map_bounds(size))
        .collect()
}
