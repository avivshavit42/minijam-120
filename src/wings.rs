use crate::character::{Character, CHARACTER_TILE_SIZE};
use crate::labels::Stages;
use crate::levels::{Levels, LoadLevel, LoadedLevel};
use crate::light::Light;
use crate::portal::PortalUi;
use crate::tile::TILE_SIZE;
use crate::utils::{is_colliding, Size as MySize, Zindex};

use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

#[derive(Component, Default)]
pub(crate) struct Wings;

pub(crate) fn spawn_wings(
    start: TilePos,
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) -> Entity {
    let texture_handle = asset_server.load("wings.png");
    let texture_atlas = TextureAtlas::from_grid(
        texture_handle,
        Vec2::new(64.0, 32.0),
        1,
        1,
        None,
        None,
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    commands
        .spawn((
            WingsBundle {
                wings: Wings,
                light: Light { strength: 18 },
                pos: start,
                ..default()
            },
            SpriteSheetBundle {
                texture_atlas: texture_atlas_handle,
                transform: Transform {
                    scale: Vec3::splat(1.),
                    translation: Vec3::new(
                        0.,
                        0.,
                        Zindex::FloorEntities.into(),
                    ),
                    ..default()
                },
                ..default()
            },
        ))
        .id()
}

pub(crate) const WINGS_TILE_SIZE: MySize = MySize {
    x: 64 / TILE_SIZE.x as u32,
    y: 32 / TILE_SIZE.y as u32,
};

#[derive(Bundle, Default)]
pub(crate) struct WingsBundle {
    pub(crate) wings: Wings,
    pub(crate) light: Light,
    pub(crate) pos: TilePos,
}

pub(crate) struct WingsPlugin;

impl Plugin for WingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(Stages::Load, wings_collision)
            .add_system(set_translation);
    }
}

fn set_translation(
    mut portals: Query<(&TilePos, &mut Transform), With<Wings>>,
    loaded_level: Res<LoadedLevel>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let center_tilemap = get_tilemap_center_transform(
        &mapsize,
        &TILE_SIZE.into(),
        &Default::default(),
        0.,
    )
    .translation;

    for (tile_pos, mut transform) in portals.iter_mut() {
        transform.translation.x =
            (tile_pos.x as f32 - 0.5) * TILE_SIZE.x + center_tilemap.x;
        transform.translation.y =
            (tile_pos.y as f32 - 0.5) * TILE_SIZE.y + center_tilemap.y;
    }
}

fn wings_collision(
    keyboard: Res<Input<KeyCode>>,
    mut character: Query<&TilePos, With<Character>>,
    wings: Query<&TilePos, With<Wings>>,
    mut event_writer: EventWriter<LoadLevel>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    ui: Query<Entity, With<PortalUi>>,
    loaded_level: Res<LoadedLevel>
) {
    let Ok(character) = character.get_single_mut() else {
        return;
    };

    for pos in &wings {
        if is_colliding(*character, CHARACTER_TILE_SIZE, *pos, WINGS_TILE_SIZE)
        {
            if keyboard.just_pressed(KeyCode::Space) {
                for ui in ui.iter() {
                    commands.entity(ui).despawn_recursive();
                }

                event_writer.send(LoadLevel(Levels::BackToMenu));

                break;
            } else if loaded_level.level == Some(Levels::Ascension) {
                if ui.get_single().is_err() {
                    commands
                        .spawn((
                            PortalUi,
                            NodeBundle {
                                style: Style {
                                    size: Size::new(
                                        Val::Percent(100.0),
                                        Val::Percent(100.0),
                                    ),
                                    position_type: PositionType::Absolute,
                                    justify_content: JustifyContent::Center,
                                    align_items: AlignItems::FlexEnd,
                                    ..Default::default()
                                },
                                ..Default::default()
                            },
                        ))
                        .with_children(|parent| {
                            parent.spawn(TextBundle {
                                text: Text {
                                    sections: vec![TextSection {
                                        value: "Space to ascend".to_string(),
                                        style: TextStyle {
                                            font_size: 150.0,
                                            font: asset_server
                                                .load("NotoSans-Regular.ttf"),
                                            ..default()
                                        },
                                    }],
                                    alignment: TextAlignment {
                                        vertical: VerticalAlign::Top,
                                        horizontal: HorizontalAlign::Center,
                                    },
                                },
                                style: Style {
                                    align_self: AlignSelf::FlexStart,
                                    ..Default::default()
                                },
                                ..Default::default()
                            });
                        });
                }
            }
        } else {
            for ui in ui.iter() {
                commands.entity(ui).despawn_recursive();
            }
        }
    }
}
