use crate::character::{Character, CHARACTER_TILE_SIZE};
use crate::enemy::Enemy;
use crate::levels::{LoadLevel, LoadedLevel};
use crate::tile::TILE_SIZE;
use crate::utils::{is_colliding, Size as MySize, Zindex};

use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

#[derive(Component, Default)]
pub(crate) struct Portal {
    pub(crate) is_open: bool,
}

pub(crate) fn spawn_portal(
    start: TilePos,
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) -> Entity {
    let texture_handle = asset_server.load("portal.png");
    let texture_atlas = TextureAtlas::from_grid(
        texture_handle,
        Vec2::new(64.0, 64.0),
        16,
        1,
        None,
        None,
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    commands
        .spawn((
            PortalBundle {
                portal: Portal { is_open: true },
                pos: start,
                ..default()
            },
            SpriteSheetBundle {
                texture_atlas: texture_atlas_handle,
                transform: Transform {
                    scale: Vec3::splat(1.),
                    translation: Vec3::new(
                        0.,
                        0.,
                        Zindex::FloorEntities.into(),
                    ),
                    ..default()
                },
                ..default()
            },
        ))
        .id()
}

pub(crate) const PORTAL_TILE_SIZE: MySize = MySize {
    x: 64 / TILE_SIZE.x as u32,
    y: 64 / TILE_SIZE.y as u32,
};

#[derive(Bundle, Default)]
pub(crate) struct PortalBundle {
    pub(crate) portal: Portal,
    pub(crate) pos: TilePos,
}

pub(crate) struct PortalPlugin;

impl Plugin for PortalPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(portal_collision)
            .add_system(set_translation)
            .add_system(animate_portal)
            .add_system(update_portal_locked_state);
    }
}

#[derive(Component)]
pub(crate) struct PortalUi;

fn portal_collision(
    keyboard: Res<Input<KeyCode>>,
    mut character: Query<&TilePos, With<Character>>,
    portals: Query<(&TilePos, &Portal)>,
    mut event_writer: EventWriter<LoadLevel>,
    loaded_level: Res<LoadedLevel>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    ui: Query<Entity, With<PortalUi>>,
) {
    let Ok(character) = character.get_single_mut() else {
        return;
    };

    for (pos, portal) in &portals {
        if is_colliding(*character, CHARACTER_TILE_SIZE, *pos, PORTAL_TILE_SIZE)
            && portal.is_open
        {
            if keyboard.just_pressed(KeyCode::Space) {
                let Some(level) = &loaded_level.level else {
                    return;
                };

                for ui in ui.iter() {
                    commands.entity(ui).despawn_recursive();
                }

                event_writer.send(LoadLevel(level.next_level()));

                break;
            }

            if ui.get_single().is_err() {
                commands
                    .spawn((
                        PortalUi,
                        NodeBundle {
                            style: Style {
                                size: Size::new(
                                    Val::Percent(100.0),
                                    Val::Percent(100.0),
                                ),
                                position_type: PositionType::Absolute,
                                justify_content: JustifyContent::Center,
                                align_items: AlignItems::FlexEnd,
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                    ))
                    .with_children(|parent| {
                        parent.spawn(TextBundle {
                            text: Text {
                                sections: vec![TextSection {
                                    value: "Space to descend".to_string(),
                                    style: TextStyle {
                                        font_size: 150.0,
                                        font: asset_server
                                            .load("NotoSans-Regular.ttf"),
                                        ..default()
                                    },
                                }],
                                alignment: TextAlignment {
                                    vertical: VerticalAlign::Top,
                                    horizontal: HorizontalAlign::Center,
                                },
                            },
                            style: Style {
                                align_self: AlignSelf::FlexStart,
                                ..Default::default()
                            },
                            ..Default::default()
                        });
                    });
            }
        } else {
            for ui in ui.iter() {
                commands.entity(ui).despawn_recursive();
            }
        }
    }
}

fn set_translation(
    mut portals: Query<(&TilePos, &mut Transform), With<Portal>>,
    loaded_level: Res<LoadedLevel>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let center_tilemap = get_tilemap_center_transform(
        &mapsize,
        &TILE_SIZE.into(),
        &Default::default(),
        0.,
    )
    .translation;

    for (tile_pos, mut transform) in portals.iter_mut() {
        transform.translation.x =
            (tile_pos.x as f32 - 0.5) * TILE_SIZE.x + center_tilemap.x;
        transform.translation.y =
            (tile_pos.y as f32 - 0.5) * TILE_SIZE.y + center_tilemap.y;
    }
}

fn animate_portal(
    time: Res<Time>,
    mut query: Query<(&mut Transform, &mut TextureAtlasSprite, &Portal)>,
) {
    for (mut transform, mut sprite, portal) in &mut query {
        let animation_speed = 0.05;

        sprite.index = (time.elapsed().as_secs_f32() / animation_speed).floor()
            as usize
            % 14;

        sprite.color = if portal.is_open {
            Color::rgb(1., 1., 1.)
        } else {
            Color::rgb(0.2, 0.2, 0.6)
        };

        let scale = if portal.is_open { 1. } else { 0.7 };

        transform.scale.x = scale;
        transform.scale.y = scale;
    }
}

fn update_portal_locked_state(
    enemies: Query<&Enemy>,
    mut portals: Query<&mut Portal>,
) {
    let is_open = enemies.iter().all(|enemy| !enemy.state.is_on_alert());

    for mut portal in &mut portals {
        portal.is_open = is_open;
    }
}
