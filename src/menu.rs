use crate::labels::Stages;
use crate::levels::{Levels, LoadLevel};
use bevy::app::AppExit;
use bevy::prelude::*;
use bevy_pkv::PkvStore;

pub(crate) struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_stage_before(
            Stages::Load,
            Stages::Menu,
            SystemStage::single_threaded(),
        )
        .add_system_to_stage(Stages::Menu, keyboard);
    }
}

#[derive(Component)]
pub(crate) struct Menu;

pub(crate) fn spawn_menu(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    has_save: bool,
) {
    let menu_text = if has_save {
        "SPACE to Continue\nR for a New Game\nESCAPE to Exit"
    } else {
        "SPACE to Start\nESCAPE to Exit"
    };

    commands
        .spawn((
            Menu,
            NodeBundle {
                style: Style {
                    size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                    display: Display::Flex,
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                ..default()
            },
        ))
        .with_children(|root| {
            root.spawn(
                TextBundle::from_section(
                    menu_text,
                    TextStyle {
                        font_size: 100.0,
                        font: asset_server.load("NotoSans-Regular.ttf"),
                        color: Color::WHITE,
                    },
                )
                .with_text_alignment(TextAlignment::TOP_CENTER),
            );
        });
}

fn keyboard(
    mut commands: Commands,
    keyboard: Res<Input<KeyCode>>,
    mut menu: Query<Entity, With<Menu>>,
    mut loader: EventWriter<LoadLevel>,
    store: Res<PkvStore>,
    mut exit: EventWriter<AppExit>,
) {
    let Ok(entity) = menu.get_single_mut() else {
        return;
    };
    if keyboard.just_pressed(KeyCode::Space) {
        commands.entity(entity).despawn_recursive();

        let level = store.get::<Levels>("level").unwrap_or(Levels::Introduction);

        loader.send(LoadLevel(level));
    } else if keyboard.just_pressed(KeyCode::R) {
        commands.entity(entity).despawn_recursive();

        loader.send(LoadLevel(Levels::Introduction));
    } else if keyboard.just_pressed(KeyCode::Escape) {
        exit.send(AppExit);
    }
}
