use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

use crate::floor::RipplePropagating;
use crate::light::{Light, LightPropagating};
use crate::wall::MovementObs;
use rand::Rng;

#[derive(Default, Component)]
pub(crate) struct Fire {
    animation_timer: Timer,
}

#[derive(Bundle, Default)]
pub(crate) struct FireBundle {
    tile: TileBundle,
    movement_obs: MovementObs,
    ripple_propagating: RipplePropagating,
    light_propagating: LightPropagating,
    fire: Fire,
    light: Light,
}

const BASE_TEXTURE_ROW: u32 = 130;

impl FireBundle {
    pub(crate) fn tile(position: TilePos, tilemap: Entity) -> Self {
        Self {
            tile: TileBundle {
                position,
                tilemap_id: TilemapId(tilemap),
                texture_index: TileTextureIndex(BASE_TEXTURE_ROW * 128),
                ..Default::default()
            },
            light: Light { strength: 4 },
            movement_obs: MovementObs {
                character: true,
                enemies: false,
            },
            ripple_propagating: RipplePropagating {
                texture_row: BASE_TEXTURE_ROW + random_frame(),
                ..default()
            },
            fire: Fire {
                animation_timer: Timer::from_seconds(
                    frametime(),
                    TimerMode::Once,
                ),
            },
            ..default()
        }
    }
}

pub(crate) struct FirePlugin;

impl Plugin for FirePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(fire_animation);
    }
}

fn frametime() -> f32 {
    let mut rng = rand::thread_rng();
    (100 + rng.gen_range(-20..21)) as f32 / 1000.
}

fn random_frame() -> u32 {
    let mut rng = rand::thread_rng();
    rng.gen_range(0..8)
}


fn fire_animation(
    time: Res<Time>,
    mut fire: Query<(&mut Fire, &mut RipplePropagating)>,
) {
    let delta = time.delta();

    for (mut fire, mut prop) in fire.iter_mut() {
        if fire.animation_timer.tick(delta).just_finished() {
            prop.texture_row = BASE_TEXTURE_ROW
                + ((prop.texture_row - BASE_TEXTURE_ROW + 1) % 8);

            fire.animation_timer =
                Timer::from_seconds(frametime(), TimerMode::Once);
        }
    }
}
