use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

use crate::floor::RipplePropagating;
use crate::wall::MovementObs;
use crate::light::LightPropagating;

#[derive(Default, Component)]
pub(crate) struct Bars;

#[derive(Bundle, Default)]
pub(crate) struct BarsBundle {
    tile: TileBundle,
    movement_obs: MovementObs,
    ripple_propagating: RipplePropagating,
    bars: Bars,
    light_prop: LightPropagating
}

impl BarsBundle {
    pub(crate) fn tile(position: TilePos, tilemap: Entity) -> Self {
        Self {
            tile: TileBundle {
                position,
                tilemap_id: TilemapId(tilemap),
                texture_index: TileTextureIndex(12),
                ..Default::default()
            },
            ripple_propagating: RipplePropagating {
                texture_row: 1,
                ..default()
            },
            ..default()
        }
    }
}
