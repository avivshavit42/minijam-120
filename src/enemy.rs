use crate::character::{Character, CHARACTER_TILE_SIZE};
use crate::floor::RipplePropagating;
use crate::levels::{LoadLevel, LoadedLevel};
use crate::light::Light;
use crate::tile::{get_neighbors, TILE_SIZE};
use crate::utils::{is_colliding, Size, Zindex};
use crate::wall::MovementObs;
use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use bevy_kira_audio::prelude::*;
use pathfinding::prelude::astar;
use rand::Rng;
use std::collections::{HashSet, VecDeque};

#[derive(Default, PartialEq, Eq, Copy, Clone, Debug)]
pub(crate) enum AlertState {
    #[default]
    Patrol,
    PreAlert,
    Alert,
    AlertWait,
}

#[derive(Default, Component)]
pub enum CharacterAnimationState {
    #[default]
    Idle,
    Up,
    Down,
    Left,
    Right,
}

impl AlertState {
    pub(crate) fn is_on_alert(self: Self) -> bool {
        match self {
            AlertState::Patrol => false,
            _ => true,
        }
    }
}

pub(crate) enum PatrolBehaviour {
    WalkTo(TilePos),
    WaitSeconds(f32),
}

#[derive(Bundle)]
pub(crate) struct EnemyBundle {
    enemy: Enemy,
    tile_pos: TilePos,
    light: Light,
}

#[derive(Component, Default)]
pub(crate) struct Enemy {
    move_timer: Timer,
    move_path: VecDeque<TilePos>,
    patrol: Vec<PatrolBehaviour>,
    patrol_step: usize,
    pub(crate) state: AlertState,
    animation_state: CharacterAnimationState,
    audio_index: u8,
}

pub(crate) struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(load_enemy_audio)
            .add_system(move_system)
            .add_system(set_translation)
            .add_system(detect_ripple)
            .add_system(animate_enemy)
            .add_system(kill_player);
    }
}

fn do_patrol_step(
    enemy: &mut Enemy,
    enemy_pos: TilePos,
    mapsize: &TilemapSize,
    movement_obstructions: &Query<(&TilePos, &MovementObs)>,
) {
    if enemy.patrol.len() > 0 {
        match &enemy.patrol[enemy.patrol_step] {
            PatrolBehaviour::WalkTo(target) => {
                if let Some(path) = plot_best_path(
                    &enemy_pos,
                    target,
                    &mapsize,
                    movement_obstructions,
                    true
                ) {
                    enemy.move_path = path.into();
                    enemy.move_timer =
                        Timer::from_seconds(PATROL_SPEED, TimerMode::Once);
                }
            }
            PatrolBehaviour::WaitSeconds(seconds) => {
                enemy.move_timer =
                    Timer::from_seconds(*seconds, TimerMode::Once);
            }
        }
    }
}

const ALERT_SPEED: f32 = 0.04;
const PATROL_SPEED: f32 = 0.1;

pub(crate) fn spawn_enemy(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    pos: TilePos,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    patrol_pattern: Vec<PatrolBehaviour>,
    mapsize: &TilemapSize,
    movement_obstructions: &Query<(&TilePos, &MovementObs)>,
) -> Entity {
    let texture_handle = asset_server.load("demon.png");
    let texture_atlas = TextureAtlas::from_grid(
        texture_handle,
        Vec2::new(96.0, 96.0),
        8,
        5,
        None,
        None,
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    let mut enemy = Enemy {
        patrol: patrol_pattern,
        ..default()
    };

    do_patrol_step(&mut enemy, pos, mapsize, movement_obstructions);

    commands
        .spawn((
            EnemyBundle {
                tile_pos: pos,
                enemy,
                light: Light { strength: 12 },
            },
            SpriteSheetBundle {
                texture_atlas: texture_atlas_handle,
                transform: Transform {
                    scale: Vec3::splat(1.),
                    translation: Vec3::new(0., 0., Zindex::Entities.into()),
                    ..default()
                },
                ..default()
            },
        ))
        .id()
}

fn kill_player(
    enemies: Query<&TilePos, With<Enemy>>,
    character: Query<&TilePos, With<Character>>,
    mut event_writer: EventWriter<LoadLevel>,
    loaded_level: Res<LoadedLevel>,
) {
    let Ok(character) = character.get_single() else {
        return;
    };

    if enemies.iter().any(|enemy| {
        is_colliding(*enemy, ENEMY_TILE_SIZE, *character, CHARACTER_TILE_SIZE)
    }) {
        let Some(level) = &loaded_level.level else {
            return;
        };

        event_writer.send(LoadLevel(*level));
    }
}

fn move_system(
    mut enemies: Query<(&mut Enemy, &mut TilePos), Without<MovementObs>>,
    time: Res<Time>,
    loaded_level: Res<LoadedLevel>,
    movement_obstructions: Query<(&TilePos, &MovementObs)>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    for (mut enemy, mut pos) in enemies.iter_mut() {
        if enemy.move_timer.tick(time.delta()).just_finished() {
            if enemy.move_path.len() > 0
                && matches!(enemy.state, AlertState::Patrol | AlertState::Alert)
            {
                let new_pos = enemy.move_path.pop_front().unwrap();
                enemy.animation_state =
                    get_enemy_animation_state(&pos, &new_pos);
                *pos = new_pos;
                enemy.move_timer.reset();
            }

            match enemy.state {
                AlertState::Patrol => {
                    if enemy.move_path.len() == 0 {
                        enemy.animation_state = CharacterAnimationState::Idle;
                        if enemy.patrol.len() > 0 {
                            enemy.patrol_step += 1;
                            enemy.patrol_step %= enemy.patrol.len();

                            do_patrol_step(
                                &mut enemy,
                                *pos,
                                &mapsize,
                                &movement_obstructions,
                            );
                        }
                    }
                }
                AlertState::PreAlert => {
                    enemy.state = AlertState::Alert;
                    enemy.move_timer =
                        Timer::from_seconds(ALERT_SPEED, TimerMode::Once);
                }
                AlertState::Alert => {
                    if enemy.move_path.len() == 0 {
                        enemy.animation_state = CharacterAnimationState::Idle;
                        enemy.state = AlertState::AlertWait;
                        enemy.move_timer =
                            Timer::from_seconds(4., TimerMode::Once);
                    }
                }
                AlertState::AlertWait => {
                    enemy.state = AlertState::Patrol;
                    enemy.move_timer =
                        Timer::from_seconds(PATROL_SPEED, TimerMode::Once);
                }
            }
        }
    }
}

const ENEMY_TILE_SIZE: Size = Size {
    x: 64 / TILE_SIZE.x as u32,
    y: 64 / TILE_SIZE.y as u32,
};

fn plot_path(
    enemy: &TilePos,
    target: &TilePos,
    mapsize: &TilemapSize,
    movement_obstructions: &Query<(&TilePos, &MovementObs)>,
    exact: bool,
) -> Option<Vec<TilePos>> {
    astar(
        enemy,
        |pos| {
            get_neighbors(pos, mapsize)
                .into_iter()
                .filter(|suc_pos: &TilePos| {
                    movement_obstructions
                        .iter()
                        .filter(|(wall_pos, obs)| {
                            obs.enemies
                                && is_colliding(
                                    *suc_pos,
                                    ENEMY_TILE_SIZE,
                                    **wall_pos,
                                    Size::SINGLE,
                                )
                        })
                        .next()
                        .is_none()
                })
                .map(|suc_pos| (suc_pos, 1))
        },
        |pos| {
            (target.x as i32 - pos.x as i32).abs()
                + (target.y as i32 - pos.y as i32).abs()
        },
        |pos| {
            if exact {
                *pos == *target
            } else {
                is_colliding(*pos, ENEMY_TILE_SIZE, *target, Size::SINGLE)
            }
        },
    )
    .map(|(mut path, _cost)| {
        path.remove(0);
        path
    })
}

fn plot_best_path(
    enemy: &TilePos,
    initial_target: &TilePos,
    mapsize: &TilemapSize,
    movement_obstructions: &Query<(&TilePos, &MovementObs)>,
    exact: bool,
) -> Option<Vec<TilePos>> {
    let mut seen: HashSet<TilePos> = Default::default();

    let mut targets: Vec<TilePos> = vec![*initial_target];

    for _ in 0..10 {
        let mut new_targets: Vec<TilePos> = vec![];

        for target in targets.iter() {
            match plot_path(
                enemy,
                target,
                mapsize,
                movement_obstructions,
                exact,
            ) {
                Some(path) => {
                    return Some(path);
                }
                None => {
                    seen.extend(targets.iter());
                    new_targets.extend(
                        get_neighbors(&target, mapsize)
                            .into_iter()
                            .collect::<HashSet<_>>()
                            .difference(&seen),
                    );
                }
            }
        }

        targets = new_targets;
    }

    None
}

fn detect_ripple(
    mut enemies: Query<(&mut Enemy, &TilePos)>,
    tiles: Query<(&TilePos, &RipplePropagating)>,
    loaded_level: Res<LoadedLevel>,
    movement_obstructions: Query<(&TilePos, &MovementObs)>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    for (mut enemy, enemy_pos) in enemies.iter_mut() {
        if let Some(ripple) = tiles
            .iter()
            .filter_map(|(pos, prop)| {
                is_colliding(*enemy_pos, ENEMY_TILE_SIZE, *pos, Size::SINGLE)
                    .then_some(&prop.ripples)
            })
            .flatten()
            .max_by_key(|ripple| ripple.dispose_timer.elapsed())
        {
            match enemy.state {
                AlertState::Alert | AlertState::PreAlert => {}
                _ => {
                    enemy.state = AlertState::PreAlert;
                    enemy.move_timer =
                        Timer::from_seconds(0.1, TimerMode::Once);
                }
            }
            if let Some(path) = plot_best_path(
                enemy_pos,
                &ripple.start,
                &mapsize,
                &movement_obstructions,
                false
            ) {
                enemy.move_path = path.into();
            } else {
                dbg!("Could not find a path!");
            }
        }
    }
}

fn get_enemy_animation_state(
    old_pos: &TilePos,
    new_pos: &TilePos,
) -> CharacterAnimationState {
    if new_pos.x > old_pos.x {
        CharacterAnimationState::Right
    } else if new_pos.x < old_pos.x {
        CharacterAnimationState::Left
    } else if new_pos.y > old_pos.y {
        CharacterAnimationState::Up
    } else if new_pos.y < old_pos.y {
        CharacterAnimationState::Down
    } else {
        CharacterAnimationState::Idle
    }
}

#[derive(Resource)]
struct EnemyAudioHandles {
    handles: Vec<Handle<AudioSource>>,
}

fn load_enemy_audio(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut audio: ResMut<DynamicAudioChannels>,
) {
    for i in 0..16 {
        audio.create_channel(format!("{}", i).as_str());
    }

    commands.insert_resource(EnemyAudioHandles {
        handles: (1..4)
            .map(|idx| asset_server.load(format!("audio/deepstep{}.ogg", idx)))
            .collect::<Vec<Handle<AudioSource>>>(),
    })
}

fn animate_enemy(
    time: Res<Time>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    mut query: Query<
        (
            &mut TextureAtlasSprite,
            &Handle<TextureAtlas>,
            &mut Enemy,
            &TilePos,
        ),
        Without<Character>,
    >,
    character_query: Query<&TilePos, (With<Character>, Without<Enemy>)>,
    audio_handles: Res<EnemyAudioHandles>,
    audio: Res<DynamicAudioChannels>,
) {
    for (mut sprite, texture_atlas_handle, mut enemy, enemy_pos) in &mut query {
        let animation_state = &enemy.animation_state;
        let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
        let animation_start = match animation_state {
            CharacterAnimationState::Idle => 0,
            CharacterAnimationState::Down => 8,
            CharacterAnimationState::Up => 16,
            CharacterAnimationState::Right => 24,
            CharacterAnimationState::Left => 32,
        };

        let idle_aniamtion_speed = 0.4;
        let movement_animation_speed = 0.06;

        let mut animation_speed = match animation_state {
            CharacterAnimationState::Idle => idle_aniamtion_speed,
            CharacterAnimationState::Down => movement_animation_speed,
            CharacterAnimationState::Up => movement_animation_speed,
            CharacterAnimationState::Right => movement_animation_speed,
            CharacterAnimationState::Left => movement_animation_speed,
        };

        if enemy.state == AlertState::Alert {
            animation_speed /= 2.;
        }

        let mut animation_offset =
            (time.elapsed().as_secs_f32() / animation_speed).floor() as usize
                % 8;

        if enemy.state == AlertState::PreAlert {
            animation_offset = 0;
        }

        let prev_animation_offset = sprite.index % 8;

        sprite.index =
            (animation_start + animation_offset) % texture_atlas.textures.len();

        if prev_animation_offset != animation_offset
            && animation_offset % 4 == 0
        {
            let volume_distance = 25.;

            let volume = if let Ok(character_pos) = character_query.get_single()
            {
                let distance = (character_pos.x as f32 - enemy_pos.x as f32)
                    .abs()
                    + (character_pos.y as f32 - enemy_pos.y as f32).abs();

                if distance < volume_distance {
                    1. - distance / volume_distance
                } else {
                    0.
                }
            } else {
                0.
            };

            let mut rng = rand::thread_rng();
            let idx = rng.gen_range(0..audio_handles.handles.len());
            let music = audio_handles.handles[idx].clone();

            enemy.audio_index += 1;
            enemy.audio_index %= 16;

            audio
                .channel(format!("{}", enemy.audio_index).as_str())
                .play(music)
                .with_volume(volume.into());
        }
    }
}

fn set_translation(
    mut enemies: Query<(&mut Enemy, &TilePos, &mut Transform)>,
    loaded_level: Res<LoadedLevel>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let center_tilemap = get_tilemap_center_transform(
        &mapsize,
        &TILE_SIZE.into(),
        &Default::default(),
        0.,
    )
    .translation;

    for (enemy, enemy_pos, mut transform) in enemies.iter_mut() {
        let move_delta =
            if enemy.move_timer.finished() || enemy.move_path.is_empty() {
                Default::default()
            } else {
                let delta_tile = Vec2 {
                    x: enemy.move_path[0].x as f32 - enemy_pos.x as f32,
                    y: enemy.move_path[0].y as f32 - enemy_pos.y as f32,
                };

                delta_tile * (1. - enemy.move_timer.percent_left())
            };

        let required_transform = Vec2::new(
            (enemy_pos.x as f32 - 0.5 + move_delta.x) * TILE_SIZE.x
                + center_tilemap.x,
            (enemy_pos.y as f32 - 0.5 + move_delta.y) * TILE_SIZE.y
                + center_tilemap.y,
        );

        if transform.translation.truncate() != required_transform + move_delta {
            transform.translation.x = required_transform.x;
            transform.translation.y = required_transform.y;
        }
    }
}
