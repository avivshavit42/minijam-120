use crate::levels::LoadedLevel;
use crate::light::LightPropagating;
use crate::tile::get_neighbors;
use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use bevy_kira_audio::prelude::*;

use multimap::MultiMap;
use rand::Rng;

#[derive(Component, Default)]
pub(crate) struct Floor;

#[derive(Component, Default)]
pub(crate) struct RipplePropagating {
    pub(crate) ripples: Vec<Ripple>,
    pub(crate) texture_row: u32,
}

#[derive(Bundle, Default)]
pub(crate) struct FloorBundle {
    tile: TileBundle,
    floor: Floor,
    ripple_propagating: RipplePropagating,
    light_prop: LightPropagating,
}

impl FloorBundle {
    pub(crate) fn tile(position: TilePos, tilemap: Entity) -> Self {
        Self {
            tile: TileBundle {
                position,
                tilemap_id: TilemapId(tilemap),
                texture_index: TileTextureIndex(0),
                ..Default::default()
            },
            ripple_propagating: RipplePropagating {
                texture_row: 0,
                ..default()
            },
            ..default()
        }
    }
}

pub(crate) struct RippleEvent {
    pub(crate) start: TilePos,
    pub(crate) ttl: u8,
}

#[derive(Default, Clone)]
pub(crate) struct Ripple {
    id: u32,
    pub(crate) start: TilePos,
    ttl: u8,
    propogate_timer: Timer,
    pub(crate) dispose_timer: Timer,
}

const RIPPLE_SPEED: f32 = 0.04;

pub(crate) struct RipplePlugin;

impl Plugin for RipplePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<RippleEvent>()
            .add_startup_system(load_move_sounds)
            .add_system(start_ripple)
            .add_system(ripple_animation)
            .add_system(propogate_ripple);
    }
}

#[derive(Resource)]
struct RippleAudioHandles {
    handles: Vec<Handle<AudioSource>>,
}

fn load_move_sounds(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(RippleAudioHandles {
        handles: (1..6)
            .map(|idx| asset_server.load(format!("audio/steps{}.ogg", idx)))
            .collect::<Vec<Handle<AudioSource>>>(),
    })
}

fn play_move_sound(
    audio: Res<Audio>,
    ripple_audio_handles: Res<RippleAudioHandles>,
) {
    let mut rng = rand::thread_rng();

    let idx = rng.gen_range(0..ripple_audio_handles.handles.len());

    let music = ripple_audio_handles.handles[idx].clone();
    audio.play(music).with_volume(0.4);
}

fn start_ripple(
    mut events: EventReader<RippleEvent>,
    mut ripple_props: Query<(&mut RipplePropagating, &TilePos)>,
    audio: Res<Audio>,
    ripple_audio_handles: Res<RippleAudioHandles>,
) {
    let mut any_ripple: bool = false;

    for ev in events.iter() {
        if let Some((mut prop, _)) = ripple_props
            .iter_mut()
            .filter(|(_, pos)| *pos == &ev.start)
            .next()
        {
            any_ripple = true;

            let mut rng = rand::thread_rng();

            let ripple = Ripple {
                id: rng.gen::<u32>(),
                start: ev.start,
                ttl: ev.ttl,
                propogate_timer: Timer::from_seconds(
                    RIPPLE_SPEED,
                    TimerMode::Once,
                ),
                dispose_timer: Timer::from_seconds(
                    RIPPLE_SPEED * 4.,
                    TimerMode::Once,
                ),
            };

            prop.ripples.push(ripple);
        }
    }

    if any_ripple {
        play_move_sound(audio, ripple_audio_handles);
    }
}

fn propogate_ripple(
    time: Res<Time>,
    mut ripple_props: Query<(&mut RipplePropagating, &TilePos)>,
    loaded_level: Res<LoadedLevel>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let delta = time.delta();

    let mut new_ripples: MultiMap<TilePos, Ripple> = Default::default();

    for (mut prop, pos) in ripple_props.iter_mut() {
        let mut ripples_to_remove: Vec<u32> = Default::default();

        for ripple in prop.ripples.iter_mut() {
            if ripple.ttl > 0 && !ripple.propogate_timer.finished() {
                if ripple.propogate_timer.tick(delta).just_finished() {
                    get_neighbors(pos, &mapsize).into_iter().for_each(|pos| {
                        new_ripples.insert(pos, ripple.clone());
                    });
                }
            }
            if !ripple.dispose_timer.finished() {
                if ripple.dispose_timer.tick(delta).just_finished() {
                    ripples_to_remove.push(ripple.id);
                }
            }
        }

        // Remove expired ripples
        prop.ripples = prop
            .ripples
            .iter()
            .filter(|ripple| !ripples_to_remove.contains(&ripple.id))
            .cloned()
            .collect::<Vec<_>>();
    }

    // Create new ripples
    for (mut prop, pos) in ripple_props.iter_mut() {
        if let Some(ripples) = new_ripples.get_vec(pos) {
            for ripple in ripples {
                if !prop.ripples.iter().any(|r| ripple.id == r.id) {
                    let mut ptimer = ripple.propogate_timer.clone();
                    ptimer.reset();

                    let mut dtimer = ripple.dispose_timer.clone();
                    dtimer.reset();

                    prop.ripples.push(Ripple {
                        ttl: ripple.ttl - 1,
                        propogate_timer: ptimer,
                        dispose_timer: dtimer,
                        ..ripple.clone()
                    });
                }
            }
        }
    }
}

fn ripple_animation(
    mut tiles: Query<(&mut TileTextureIndex, &RipplePropagating)>,
) {
    for (mut texture, prop) in tiles.iter_mut() {
        let offset = prop.texture_row * 128;

        *texture = if prop.ripples.len() == 0 {
            TileTextureIndex(0 + offset)
        } else {
            let max_duration = prop.ripples[0].dispose_timer.duration();

            let min_timer = prop
                .ripples
                .iter()
                .map(|ripple| ripple.dispose_timer.elapsed())
                .max()
                .unwrap();

            let frames = vec![1, 2, 3, 4, 4, 3, 3, 2, 2, 1, 1];

            let frametime = max_duration / frames.len() as u32;

            let animation_index = frames[(min_timer.as_secs_f32()
                / frametime.as_secs_f32())
            .floor() as usize]
                + offset;

            TileTextureIndex(animation_index)
        };
    }
}
