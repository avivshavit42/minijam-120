use crate::levels::LoadedLevel;
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use bevy_ecs_tilemap::prelude::*;
use std::collections::{HashMap, HashSet};

pub(crate) struct LightPlugin;

impl Plugin for LightPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(light_system);
    }
}

#[derive(Component, Default)]
pub(crate) struct ViewSource;

#[derive(Component, Default)]
pub(crate) struct Light {
    pub(crate) strength: usize,
}

#[derive(Component, Default)]
pub(crate) struct LightPropagating;

fn line_of_sight(
    start: TilePos,
    light_props: &HashSet<TilePos>,
    distance: Option<usize>,
    mapsize: TilemapSize,
) -> HashMap<TilePos, f32> {
    let mut ret: HashMap<TilePos, f32> = Default::default();

    let mut angle = 0.;
    while angle < std::f32::consts::TAU {
        let mut t: f32 = 0.;

        loop {
            if let Some(distance) = distance && t > distance as f32 {
                break;
            }

            let current_pos = TilePos {
                x: (start.x as f32 + t * angle.cos()).round() as u32,
                y: (start.y as f32 + t * angle.sin()).round() as u32,
            };

            if !current_pos.within_map_bounds(&mapsize) {
                break;
            }

            if !light_props.contains(&current_pos) {
                ret.insert(current_pos, t);
                break;
            }

            ret.insert(current_pos, t);
            t += 0.5;
        }

        angle += 0.5 / distance.unwrap_or(100) as f32;
    }

    ret
}

fn light_system(
    lights: Query<(&TilePos, &Light)>,
    view_sources: Query<&TilePos, With<ViewSource>>,
    entities: Query<(Entity, &TilePos)>,
    mut to_darken: Query<(Entity, &mut TextureAtlasSprite), Without<Light>>,
    light_propagating: Query<&TilePos, With<LightPropagating>>,
    loaded_level: Res<LoadedLevel>,
    tile_storage: Query<&TileStorage>,
    mut tile_vis: Query<&mut TileColor>,
    mut commands: Commands,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let lp = light_propagating.iter().copied().collect::<HashSet<_>>();
    let mut lit_tiles: HashMap<TilePos, f32> = Default::default();

    for (light_pos, light) in lights.iter() {
        for (pos, t) in
            line_of_sight(*light_pos, &lp, Some(light.strength), mapsize)
                .iter()
                .map(|(pos, t)| {
                    (*pos, 1. - (t / std::cmp::max(light.strength, 1) as f32))
                })
        {
            if let Some(old_t) = lit_tiles.get(&pos) {
                if *old_t >= t {
                    continue;
                }
            }

            lit_tiles.insert(pos, t);
        }
    }

    let mut visible_tiles: HashMap<TilePos, f32> = Default::default();

    for vis_pos in view_sources.iter() {
        visible_tiles.extend(line_of_sight(*vis_pos, &lp, None, mapsize));
    }

    let Ok(tile_storage) = tile_storage.get_single() else {
        return;
    };

    let final_tiles = lit_tiles
        .into_iter()
        .filter(|(pos, _)| visible_tiles.contains_key(pos))
        .collect::<HashMap<TilePos, f32>>();

    let mut to_darken = to_darken.iter_mut().collect::<HashMap<_, _>>();

    for (entity, pos) in entities.iter() {
        if let Some(tile) = tile_storage.get(pos) {
            if entity == tile {
                let mut color = tile_vis.get_mut(tile).unwrap();
                if let Some(t) = final_tiles.get(pos) {
                    color.0 = Color::rgb(*t, *t - 0.2, *t - 0.2);
                } else {
                    color.0 = Color::BLACK;
                }
            } else {
                if let Some(t) = final_tiles.get(pos) {
                    commands.entity(entity).insert(RenderLayers::layer(0));
                    if let Some(sprite) = to_darken.get_mut(&entity) {
                        sprite.color = Color::rgb(*t, *t - 0.2, *t - 0.2);
                    }
                } else {
                    commands.entity(entity).insert(RenderLayers::layer(1));
                }
            }
        }
    }
}
