#![feature(let_chains)]

use bevy::log::LogPlugin;
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use bevy_kira_audio::prelude::*;
use bevy_pkv::PkvStore;

mod wings;
use wings::WingsPlugin;

mod utils;

mod menu;
use menu::{MenuPlugin, spawn_menu};

mod light;
use light::LightPlugin;

mod labels;

mod bars;
mod fire;
use fire::FirePlugin;

mod enemy;
use enemy::EnemyPlugin;

mod levels;
use levels::{LevelPlugin, Levels, IsIntroPlaying};

mod floor;
use floor::RipplePlugin;

mod portal;
use portal::PortalPlugin;

mod wall;

mod character;
use character::CharacterPlugin;

mod tile;
use tile::TilePlugin;

#[derive(Component)]
struct ParallaxSprite {
    ratio: f32,
}

fn parallax_movement(
    mut sprites: Query<(&mut Transform, &ParallaxSprite), Without<Camera>>,
    cameras: Query<&Transform, (With<Camera>, Without<ParallaxSprite>)>,
) {
    let Ok(camera) = cameras.get_single() else {
        return;
    };

    sprites
        .iter_mut()
        .for_each(|(mut transform, parallax_sprite)| {
            transform.translation.x =
                camera.translation.x * (1. - parallax_sprite.ratio);
            transform.translation.y =
                camera.translation.y * (1. - parallax_sprite.ratio);
        });
}

#[derive(Component)]
struct Background;

fn setup(mut commands: Commands, asset_server: Res<AssetServer>,
         ) {
    commands.spawn((Camera2dBundle::default(), RenderLayers::layer(0)));

    let store = PkvStore::new("Undeaf", "Undeaf");
    let has_save = store.get::<Levels>("level").is_ok();

    commands.insert_resource(store);

    spawn_menu(&mut commands, &asset_server, has_save);

    commands.insert_resource(IsIntroPlaying(false));
}

#[derive(Resource)]
struct BackgroundMusic;

fn game_music(
    asset_server: Res<AssetServer>,
    audio: Res<AudioChannel<BackgroundMusic>>,
) {
    let music = asset_server.load("audio/ScaryAmbient.ogg");
    audio.play(music).looped();
}

fn main() {
    App::new()
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    window: WindowDescriptor {
                        title: String::from("Undeaf"),
                        resizable: false,
                        mode: WindowMode::Fullscreen,
                        ..default()
                    },
                    ..default()
                })
                 .set(LogPlugin {
                     filter: "info,wgpu_core=warn,wgpu_hal=warn,winit=warn,bevy_render=warn,symphonia_core=warn,symphonia_format_ogg=warn".to_string(),
                     level: bevy::log::Level::DEBUG
                 }))
        .insert_resource(ClearColor(Color::BLACK))
        .add_plugin(AudioPlugin)
        .add_audio_channel::<BackgroundMusic>()
        .add_startup_system(setup)
        .add_startup_system(game_music)
        .add_system(parallax_movement)
        .add_plugin(TilePlugin)
        .add_plugin(CharacterPlugin)
        .add_plugin(RipplePlugin)
        .add_plugin(EnemyPlugin)
        .add_plugin(LevelPlugin)
        .add_plugin(PortalPlugin)
        .add_plugin(LightPlugin)
        .add_plugin(MenuPlugin)
        .add_plugin(WingsPlugin)
        .add_plugin(FirePlugin)
        //.add_plugin(LogDiagnosticsPlugin::default())
        //.add_plugin(FrameTimeDiagnosticsPlugin::default())
        .run();
}
