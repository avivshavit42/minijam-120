use crate::floor::RippleEvent;
use crate::utils::{is_colliding, Size, Zindex};
use crate::wall::MovementObs;
use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

use crate::levels::LoadedLevel;
use crate::light::{Light, ViewSource};
use crate::tile::TILE_SIZE;

#[derive(Bundle, Default)]
pub(crate) struct CharacterBundle {
    pub(crate) character: Character,
    pub(crate) light: Light,
    pub(crate) pos: TilePos,
    pub(crate) viewsource: ViewSource,
}

#[derive(Component, Default)]
pub(crate) struct Character {
    pub(crate) move_delta: Vec2,
    pub(crate) ripple_timer: Timer,
    pub(crate) last_ripple: TilePos,
    animation_direction: CharacterDirection,
    animation_state: CharacterIdleOrWalking,
}

#[derive(Default, Component)]
pub enum CharacterIdleOrWalking {
    #[default]
    Idle,
    Walking,
}

#[derive(Default, Component)]
pub enum CharacterDirection {
    #[default]
    Up,
    Down,
    Left,
    Right,
}

const MOVE_SPEED: f32 = 150.0;

pub(crate) const CHARACTER_TILE_SIZE: Size = Size {
    x: 32 / TILE_SIZE.x as u32,
    y: 32 / TILE_SIZE.y as u32,
};

pub(crate) struct CharacterPlugin;

impl Plugin for CharacterPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(movement)
            .add_system(set_translation.after(movement))
            .add_system(focus_camera.after(set_translation))
            .add_system(animate_character);
    }
}

pub(crate) fn spawn_character(
    start: TilePos,
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) -> Entity {
    let texture_handle = asset_server.load("angel.png");
    let texture_atlas = TextureAtlas::from_grid(
        texture_handle,
        Vec2::new(64.0, 98.0),
        20,
        1,
        None,
        None,
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    commands
        .spawn((
            CharacterBundle {
                character: Character {
                    ripple_timer: Timer::from_seconds(0.5, TimerMode::Once),
                    last_ripple: start,
                    ..default()
                },
                pos: start,
                light: Light { strength: 6 },
                ..default()
            },
            SpriteSheetBundle {
                texture_atlas: texture_atlas_handle,
                transform: Transform {
                    scale: Vec3::splat(1.),
                    translation: Vec3::new(0., 0., Zindex::Entities.into()),
                    ..default()
                },
                ..default()
            },
        ))
        .id()
}

fn set_translation(
    mut character: Query<(&mut Character, &TilePos, &mut Transform)>,
    mut ripple_writer: EventWriter<RippleEvent>,
    loaded_level: Res<LoadedLevel>,
    time: Res<Time>,
) {
    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let Ok((mut character, tile_pos, mut transform)) =
        character.get_single_mut() else {
            return;
        };

    let center_tilemap = get_tilemap_center_transform(
        &mapsize,
        &TILE_SIZE.into(),
        &Default::default(),
        0.,
    )
    .translation;

    let required_transform = Vec2::new(
        (tile_pos.x as f32 - 0.5) * TILE_SIZE.x + center_tilemap.x,
        (tile_pos.y as f32 - 0.5) * TILE_SIZE.y + center_tilemap.y,
    );

    character.ripple_timer.tick(time.delta());

    if character.ripple_timer.finished() && character.last_ripple != *tile_pos {
        character.ripple_timer.reset();

        let mut ripple_start = *tile_pos;

        if character.move_delta.x < 0. {
            ripple_start.x -= 1;
        }
        if character.move_delta.y < 0. {
            ripple_start.y -= 1;
        }

        ripple_writer.send(RippleEvent {
            start: ripple_start,
            ttl: 6,
        });
        character.last_ripple = *tile_pos;
    }

    let old_transform = transform.translation.truncate();
    let new_transform = required_transform + character.move_delta;

    let new_direction = if new_transform.x > old_transform.x {
        Some(CharacterDirection::Right)
    } else if new_transform.x < old_transform.x {
        Some(CharacterDirection::Left)
    } else if new_transform.y > old_transform.y {
        Some(CharacterDirection::Up)
    } else if new_transform.y < old_transform.y {
        Some(CharacterDirection::Down)
    } else {
        None
    };

    if let Some(new_direction) = new_direction {
        character.animation_direction = new_direction;
    }

    if old_transform == new_transform {
        character.animation_state = CharacterIdleOrWalking::Idle;
    } else {
        character.animation_state = CharacterIdleOrWalking::Walking;
        transform.translation.x = required_transform.x + character.move_delta.x;
        transform.translation.y = required_transform.y + character.move_delta.y;
    }
}

fn animate_character(
    time: Res<Time>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    mut query: Query<(
        &mut TextureAtlasSprite,
        &Handle<TextureAtlas>,
        &Character,
    )>,
) {
    for (mut sprite, texture_atlas_handle, character) in &mut query {
        let animation_state = &character.animation_state;
        let animation_direction = &character.animation_direction;
        let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
        let animation_start = match animation_direction {
            CharacterDirection::Down => 0,
            CharacterDirection::Up => 4,
            CharacterDirection::Right => 8,
            CharacterDirection::Left => 12,
        };

        let animation_offset = match animation_state {
            CharacterIdleOrWalking::Idle => 0,
            CharacterIdleOrWalking::Walking => {
                (time.elapsed().as_secs_f32() / 0.2).floor() as usize % 4
            }
        };

        sprite.index =
            (animation_start + animation_offset) % texture_atlas.textures.len();
    }
}

fn movement(
    keyboard: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut character: Query<(&mut Character, &mut TilePos)>,
    walls: Query<(&TilePos, &MovementObs), Without<Character>>,
    loaded_level: Res<LoadedLevel>,
) {
    let Ok((mut character, mut tile_pos)) = character.get_single_mut() else {
        return;
    };

    let Some(mapsize) = loaded_level.mapsize else {
        return;
    };

    let right_pressed =
        keyboard.pressed(KeyCode::D) || keyboard.pressed(KeyCode::Right);

    let left_pressed =
        keyboard.pressed(KeyCode::A) || keyboard.pressed(KeyCode::Left);

    let up_pressed =
        keyboard.pressed(KeyCode::W) || keyboard.pressed(KeyCode::Up);

    let down_pressed =
        keyboard.pressed(KeyCode::S) || keyboard.pressed(KeyCode::Down);

    let key_count = right_pressed as u32
        + left_pressed as u32
        + up_pressed as u32
        + down_pressed as u32;

    let distance = time.delta().as_secs_f32() * MOVE_SPEED / (key_count as f32);

    if right_pressed {
        character.move_delta.x += distance;
    }

    if left_pressed {
        character.move_delta.x -= distance;
    }

    if up_pressed {
        character.move_delta.y += distance;
    }

    if down_pressed {
        character.move_delta.y -= distance;
    }

    if !(keyboard.pressed(KeyCode::W)
        || keyboard.pressed(KeyCode::A)
        || keyboard.pressed(KeyCode::S)
        || keyboard.pressed(KeyCode::D))
    {
        let duration = character.ripple_timer.duration();
        character.ripple_timer.tick(duration);
    }

    // Sorry in advance

    // Going right
    while character.move_delta.x > TILE_SIZE.x / 2. {
        character.move_delta.x -= TILE_SIZE.x;

        let next_pos = TilePos {
            x: tile_pos.x + 1,
            y: tile_pos.y,
        };

        if tile_pos.x >= mapsize.x - 1
            || walls.iter().any(|(wall_pos, obs)| {
                obs.character
                    && is_colliding(
                        next_pos,
                        CHARACTER_TILE_SIZE,
                        *wall_pos,
                        Size::SINGLE,
                    )
            })
        {
            character.move_delta.x = TILE_SIZE.x / 2. - 1e-5;
            break;
        }

        *tile_pos = next_pos;
    }

    // Going left
    while character.move_delta.x < -TILE_SIZE.x / 2. {
        character.move_delta.x += TILE_SIZE.x;

        let next_pos = TilePos {
            x: tile_pos.x - 1,
            y: tile_pos.y,
        };

        if tile_pos.x <= 0
            || walls.iter().any(|(wall_pos, obs)| {
                obs.character
                    && is_colliding(
                        next_pos,
                        CHARACTER_TILE_SIZE,
                        *wall_pos,
                        Size::SINGLE,
                    )
            })
        {
            character.move_delta.x = -TILE_SIZE.x / 2. + 1e-5;
            break;
        }

        *tile_pos = next_pos;
    }

    // Going up
    while character.move_delta.y > TILE_SIZE.y / 2. {
        character.move_delta.y -= TILE_SIZE.y;

        let next_pos = TilePos {
            x: tile_pos.x,
            y: tile_pos.y + 1,
        };

        if tile_pos.y >= mapsize.y - 1
            || walls.iter().any(|(wall_pos, obs)| {
                obs.character
                    && is_colliding(
                        next_pos,
                        CHARACTER_TILE_SIZE,
                        *wall_pos,
                        Size::SINGLE,
                    )
            })
        {
            character.move_delta.y = TILE_SIZE.y / 2. - 1e-5;
            break;
        }

        *tile_pos = next_pos;
    }

    // Going down
    while character.move_delta.y < -TILE_SIZE.y / 2. {
        character.move_delta.y += TILE_SIZE.y;

        let next_pos = TilePos {
            x: tile_pos.x,
            y: tile_pos.y - 1,
        };

        if tile_pos.y <= 0
            || walls.iter().any(|(wall_pos, obs)| {
                obs.character
                    && is_colliding(
                        next_pos,
                        CHARACTER_TILE_SIZE,
                        *wall_pos,
                        Size::SINGLE,
                    )
            })
        {
            character.move_delta.y = -TILE_SIZE.y / 2. + 1e-5;
            break;
        }

        *tile_pos = next_pos;
    }
}

fn focus_camera(
    character: Query<&Transform, (With<Character>, Without<Camera>)>,
    mut camera: Query<&mut Transform, With<Camera>>,
) {
    let Ok(char_transform) = character.get_single() else {
        return;
    };

    let Ok(mut camera_transform) = camera.get_single_mut() else {
        return;
    };

    camera_transform.translation = char_transform.translation;
}
