use bevy::asset::{AssetLoader, LoadContext, LoadedAsset};
use bevy::prelude::*;
use bevy::reflect::TypeUuid;
use bevy::utils::BoxedFuture;
use bevy_ecs_tilemap::prelude::*;
use bevy_kira_audio::prelude::*;
use bevy_pkv::PkvStore;
use serde::{Deserialize, Serialize};

use crate::bars::BarsBundle;
use crate::character::spawn_character;
use crate::enemy::{spawn_enemy, PatrolBehaviour};
use crate::fire::FireBundle;
use crate::floor::FloorBundle;
use crate::labels::Stages;
use crate::menu::spawn_menu;
use crate::portal::{spawn_portal, PortalUi};
use crate::tile::TILE_SIZE;
use crate::utils::Zindex;
use crate::wall::{MovementObs, WallBundle};
use crate::wings::spawn_wings;
use std::collections::HashMap;

pub(crate) struct LevelPlugin;

#[derive(Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub(crate) enum Levels {
    Introduction,
    Ropes,
    Junctions,
    Patience,
    Heat,
    Trigger,
    Ascension,
    BackToMenu,
}

#[derive(Component)]
pub(crate) struct Intro;

#[derive(Resource)]
pub(crate) struct IsIntroPlaying(pub(crate) bool);

#[derive(Resource, Default)]
pub(crate) struct LoadedLevel {
    pub(crate) level: Option<Levels>,
    pub(crate) entities: Vec<Entity>,
    pub(crate) intro_timer: Option<Timer>,
    pub(crate) intro_step: Option<usize>,
    pub(crate) mapsize: Option<TilemapSize>,
    pub(crate) csv_handle: Handle<CsvAsset>,
}

impl LoadedLevel {
    pub(crate) fn despawn(&mut self, commands: &mut Commands) {
        for entity in self.entities.iter().copied() {
            commands.entity(entity).despawn_recursive();
        }

        self.entities = vec![];
        self.level = None;
        self.intro_step = None;
        self.intro_timer = None;
        self.mapsize = None;
    }
}

fn spawn_intro(
    text: &str,
    color: Color,
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
) {
    commands
        .spawn((
            Intro,
            NodeBundle {
                style: Style {
                    size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                    display: Display::Flex,
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                ..default()
            },
        ))
        .with_children(|root| {
            root.spawn(
                TextBundle::from_section(
                    text,
                    TextStyle {
                        font_size: 100.0,
                        font: asset_server.load("NotoSans-Regular.ttf"),
                        color,
                    },
                )
                .with_text_alignment(TextAlignment::TOP_CENTER),
            );
        });
}

#[derive(Debug, Deserialize, TypeUuid)]
#[uuid = "39cadc56-aa9c-4543-8640-a018b74b5052"]
pub(crate) struct CsvAsset(String);

impl Levels {
    fn name(&self) -> &str {
        match self {
            Self::Introduction => "introduction",
            Self::Ropes => "ropes",
            Self::Junctions => "junctions",
            Self::Patience => "patience",
            Self::Heat => "heat",
            Self::Trigger => "trigger",
            Self::Ascension => "ascension",
            Self::BackToMenu => "introduction",
        }
    }

    fn intro(&self) -> Vec<(&str, f32, Color, Color)> {
        match self {
            Self::Introduction => vec![
                ("I am an Angel", 2., Color::WHITE, Color::BLACK),
                ("They took my wings", 2., Color::WHITE, Color::BLACK),
                ("To the depths", 2., Color::WHITE, Color::BLACK),
                ("Of Hell", 3.5, Color::WHITE, Color::BLACK),
            ],
            Self::Ropes => vec![
                ("They can't see me", 2., Color::WHITE, Color::BLACK),
                ("But they can hear me", 3., Color::WHITE, Color::BLACK),
            ],
            Self::Junctions => vec![
                ("My wings", 2., Color::WHITE, Color::BLACK),
                ("I must find them", 3., Color::WHITE, Color::BLACK),
            ],
            Self::Patience => {
                vec![("Patience", 3., Color::WHITE, Color::BLACK)]
            }
            Self::Heat => vec![
                (
                    "They will burn",
                    1.5,
                    Color::rgb(0.8, 0.2, 0.2),
                    Color::BLACK,
                ),
                (
                    "For what they did",
                    1.5,
                    Color::rgb(0.8, 0.2, 0.2),
                    Color::BLACK,
                ),
            ],
            Self::Trigger => {
                vec![("I am not afraid", 3., Color::WHITE, Color::BLACK)]
            }
            Self::Ascension => {
                vec![
                    ("The light", 2., Color::WHITE, Color::BLACK),
                    ("The end of the tunnel", 3., Color::WHITE, Color::BLACK),
                    ("Is this The End?", 2., Color::WHITE, Color::BLACK),
                ]
            }
            Self::BackToMenu => vec![
                (
                    "I descended to the depths",
                    3.,
                    Color::rgb(0.21, 0.39, 0.73),
                    Color::WHITE,
                ),
                (
                    "I found my wings",
                    3.,
                    Color::rgb(0.21, 0.39, 0.73),
                    Color::WHITE,
                ),
                ("I Ascended", 3., Color::rgb(0.21, 0.39, 0.73), Color::WHITE),
                ("But have I lost myself?", 3., Color::BLACK, Color::WHITE),
                ("", 5., Color::WHITE, Color::BLACK),
                ("Alon Eldan", 1., Color::WHITE, Color::BLACK),
                ("Aviel Boag", 1., Color::WHITE, Color::BLACK),
                ("Aviv Shavit", 1., Color::WHITE, Color::BLACK),
                ("Boaz Guberman", 1., Color::WHITE, Color::BLACK),
            ],
        }
    }

    pub(crate) fn next_level(&self) -> Self {
        match self {
            Self::Introduction => Self::Ropes,
            Self::Ropes => Self::Junctions,
            Self::Junctions => Self::Patience,
            Self::Patience => Self::Heat,
            Self::Heat => Self::Trigger,
            Self::Trigger => Self::Ascension,
            Self::Ascension => Self::BackToMenu,
            Self::BackToMenu => unreachable!(),
        }
    }

    fn spawn(
        &self,
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
        loaded_level: &mut LoadedLevel,
        movement_obstructions: &Query<(&TilePos, &MovementObs)>,
        csv: &String,
    ) {
        let texture_handle: Handle<Image> = asset_server.load("tiles.png");

        let tilemap_entity = commands.spawn_empty().id();

        loaded_level.entities.push(tilemap_entity);

        let mut rdr = csv::ReaderBuilder::new().from_reader(csv.as_bytes());

        let headers = rdr.headers().unwrap();
        let tilemap_size = TilemapSize {
            y: headers[0].parse().unwrap(),
            x: headers[1].parse().unwrap(),
        };

        loaded_level.mapsize = Some(tilemap_size);

        let mut tile_storage = TileStorage::empty(tilemap_size);

        let mut player_start: Option<TilePos> = None;
        let mut portal_pos: Option<TilePos> = None;
        let mut wings_pos: Option<TilePos> = None;

        let mut number_tiles: HashMap<u32, TilePos> = Default::default();

        let mut record_iter = rdr.records().enumerate();

        while let Some((y, row)) = record_iter.next() {
            let record: csv::StringRecord = row.unwrap();

            if record.iter().next() == Some("END") {
                break;
            }

            for (x, cell) in record.iter().enumerate() {
                let tile_pos = TilePos {
                    x: x as u32,
                    y: tilemap_size.y - (y as u32) - 1,
                };
                let tile_entity = if cell == "F" {
                    commands.spawn(FloorBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "W" {
                    commands.spawn(WallBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "B" {
                    commands.spawn(BarsBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "R" {
                    commands.spawn(FireBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "P" {
                    portal_pos = Some(tile_pos);
                    commands.spawn(FloorBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "S" {
                    player_start = Some(tile_pos);
                    commands.spawn(FloorBundle::tile(tile_pos, tilemap_entity))
                } else if cell == "N" {
                    wings_pos = Some(tile_pos);
                    commands.spawn(FloorBundle::tile(tile_pos, tilemap_entity))
                } else if let Ok(num) = cell.parse::<u32>() {
                    number_tiles.insert(num, tile_pos);
                    commands.spawn(FloorBundle::tile(tile_pos, tilemap_entity))
                } else {
                    unreachable!()
                }
                .id();

                tile_storage.set(&tile_pos, tile_entity);
                loaded_level.entities.push(tile_entity);
            }
        }

        while let Some((_, row)) = record_iter.next() {
            let record: csv::StringRecord = row.unwrap();

            let mut patrol: Vec<PatrolBehaviour> = vec![];

            let mut cells = record.iter();

            let start =
                number_tiles[&cells.next().unwrap().parse::<u32>().unwrap()];

            while let Some(cell) = cells.next() {
                if cell == "" {
                    break;
                } else if cell.chars().next() == Some('W') {
                    let time = cell[1..].parse::<f32>().unwrap();
                    patrol.push(PatrolBehaviour::WaitSeconds(time));
                } else {
                    let pos = cell.parse::<u32>().unwrap();
                    patrol.push(PatrolBehaviour::WalkTo(number_tiles[&pos]));
                }
            }

            loaded_level.entities.push(spawn_enemy(
                commands,
                &asset_server,
                start,
                texture_atlases,
                patrol,
                &tilemap_size,
                &movement_obstructions,
            ));
        }

        let player_start = match player_start {
            Some(start) => start,
            None => TilePos { x: 5, y: 10 },
        };

        if let Some(portal_pos) = portal_pos {
            loaded_level.entities.push(spawn_portal(
                portal_pos,
                commands,
                asset_server,
                texture_atlases,
            ));
        }

        if let Some(wings_pos) = wings_pos {
            loaded_level.entities.push(spawn_wings(
                wings_pos,
                commands,
                asset_server,
                texture_atlases,
            ));
        }

        loaded_level.entities.push(spawn_character(
            player_start,
            commands,
            asset_server,
            texture_atlases,
        ));

        let grid_size = TILE_SIZE.into();
        let map_type = TilemapType::default();

        let tilemap_transform = get_tilemap_center_transform(
            &tilemap_size,
            &grid_size,
            &map_type,
            Zindex::Tiles.into(),
        );

        commands.entity(tilemap_entity).insert(TilemapBundle {
            grid_size,
            map_type,
            size: tilemap_size,
            storage: tile_storage,
            texture: TilemapTexture::Single(texture_handle),
            tile_size: TILE_SIZE,
            transform: tilemap_transform,
            ..Default::default()
        });
    }
}

pub(crate) struct LoadLevel(pub(crate) Levels);

fn setup_level_resources(mut commands: Commands) {
    commands.insert_resource(LoadedLevel::default());
}

fn intro(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut loaded_level: ResMut<LoadedLevel>,
    intro: Query<Entity, With<Intro>>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    movement_obstructions: Query<(&TilePos, &MovementObs)>,
    time: Res<Time>,
    mut store: ResMut<PkvStore>,
    mut is_intro: ResMut<IsIntroPlaying>,
    csvs: Res<Assets<CsvAsset>>,
    mut bg: ResMut<ClearColor>,
) {
    let Some(csv) = csvs.get(&loaded_level.csv_handle) else {
        return;
    };

    if let Some(step) = loaded_level.intro_step {
        if let Some(level) = loaded_level.level {
            if let Some(timer) = &mut loaded_level.intro_timer {
                if timer.tick(time.delta()).just_finished() {
                    if let Ok(intro) = intro.get_single() {
                        commands.entity(intro).despawn_recursive();
                    }

                    if let Some((text, intro_time, color, bg_color)) =
                        level.intro().get(step + 1)
                    {
                        bg.0 = *bg_color;
                        loaded_level.intro_step = Some(step + 1);
                        spawn_intro(text, *color, &mut commands, &asset_server);
                        loaded_level.intro_timer = Some(Timer::from_seconds(
                            *intro_time,
                            TimerMode::Once,
                        ));
                    } else {
                        bg.0 = Color::BLACK;
                        is_intro.0 = false;
                        if level == Levels::BackToMenu {
                            if let Err(_) = store.clear() {
                                dbg!("Faild to clear saves");
                            }

                            back_to_menu(
                                &mut loaded_level,
                                &mut commands,
                                &intro,
                                &asset_server,
                                false,
                            );
                        } else {
                            loaded_level.intro_step = None;
                            loaded_level.intro_timer = None;

                            level.spawn(
                                &mut commands,
                                &asset_server,
                                &mut texture_atlases,
                                &mut loaded_level,
                                &movement_obstructions,
                                &csv.0,
                            );
                        }
                    }
                }
            } else {
                // Spawn intro and start timer
                let (text, intro_time, color, bg_color) = level.intro()[step];
                bg.0 = bg_color;
                spawn_intro(text, color, &mut commands, &asset_server);
                loaded_level.intro_timer =
                    Some(Timer::from_seconds(intro_time, TimerMode::Once));
            }
        }
    }
}

#[derive(Resource)]
struct LevelStartMusic {
    handle: Handle<AudioSource>,
}

fn load_level_start_sound(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands.insert_resource(LevelStartMusic {
        handle: asset_server.load("audio/found.ogg"),
    });
}

fn load_level(
    mut commands: Commands,
    mut loaded_level: ResMut<LoadedLevel>,
    mut ev: EventReader<LoadLevel>,
    mut store: ResMut<PkvStore>,
    mut is_intro: ResMut<IsIntroPlaying>,
    asset_server: Res<AssetServer>,
    audio: Res<Audio>,
    level_start_music: Res<LevelStartMusic>,
) {
    let Some(level_ev) = ev.iter().next() else {
        return;
    };
    loaded_level.despawn(&mut commands);

    audio
        .play(level_start_music.handle.clone())
        .with_volume(1.5);

    loaded_level.level = Some(level_ev.0);
    loaded_level.intro_step = Some(0);

    loaded_level.csv_handle = asset_server
        .load::<CsvAsset, String>(format!("levels/{}.csv", level_ev.0.name()));
    is_intro.0 = true;
    if let Err(_) = store.set("level", &level_ev.0) {
        dbg!("Faild to save level");
    }
}

pub(crate) fn back_to_menu(
    loaded_level: &mut LoadedLevel,
    commands: &mut Commands,
    intro: &Query<Entity, With<Intro>>,
    asset_server: &Res<AssetServer>,
    has_save: bool,
) {
    loaded_level.despawn(commands);
    if let Ok(intro) = intro.get_single() {
        commands.entity(intro).despawn_recursive();
    }
    spawn_menu(commands, asset_server, has_save);
}

fn reset_level_or_exit(
    keyboard: Res<Input<KeyCode>>,
    mut loaded_level: ResMut<LoadedLevel>,
    intro: Query<Entity, With<Intro>>,
    mut event_writer: EventWriter<LoadLevel>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    store: Res<PkvStore>,
    is_intro: Res<IsIntroPlaying>,
    ui: Query<Entity, With<PortalUi>>,
) {
    if let Some(level) = loaded_level.level {
        if keyboard.just_pressed(KeyCode::R) {
            if !is_intro.0 {
                for ui in ui.iter() {
                    commands.entity(ui).despawn_recursive();
                }
                event_writer.send(LoadLevel(level));
            }
        } else if keyboard.just_pressed(KeyCode::Escape) {
            if !is_intro.0 {
                for ui in ui.iter() {
                    commands.entity(ui).despawn_recursive();
                }

                back_to_menu(
                    &mut loaded_level,
                    &mut commands,
                    &intro,
                    &asset_server,
                    store.get::<Levels>("level").is_ok(),
                );
            }
        }
    }
}

#[derive(Default)]
pub struct CsvLoader;

impl AssetLoader for CsvLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), bevy::asset::Error>> {
        Box::pin(async move {
            load_context.set_default_asset(LoadedAsset::new(CsvAsset(
                std::str::from_utf8(bytes).unwrap().to_string(),
            )));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["csv"]
    }
}

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_level_resources)
            .add_stage_before(
                CoreStage::Update,
                Stages::Load,
                SystemStage::single_threaded(),
            )
            .add_system_to_stage(Stages::Load, load_level)
            .add_system_to_stage(
                Stages::Load,
                reset_level_or_exit.before(load_level),
            )
            .add_system_to_stage(Stages::Load, intro)
            .add_event::<LoadLevel>()
            .add_asset::<CsvAsset>()
            .add_startup_system(load_level_start_sound)
            .init_asset_loader::<CsvLoader>();
    }
}
