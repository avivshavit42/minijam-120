use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

#[derive(Component)]
pub(crate) struct MovementObs {
    pub(crate) character: bool,
    pub(crate) enemies: bool
}

impl Default for MovementObs {
    fn default() -> Self {
        Self {
            character: true,
            enemies: true
        }
    }
}

#[derive(Bundle, Default)]
pub(crate) struct WallBundle {
    tile: TileBundle,
    movement_obs: MovementObs,
}

impl WallBundle {
    pub(crate) fn tile(position: TilePos, tilemap: Entity) -> Self {
        Self {
            tile: TileBundle {
                position,
                tilemap_id: TilemapId(tilemap),
                texture_index: TileTextureIndex(
                    256 + position.x % 128 + (position.y % 128) * 128,
                ),
                ..Default::default()
            },
            ..default()
        }
    }
}
